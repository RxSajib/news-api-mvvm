package com.news.newsappmvvm.Data;

public class DataManager {

    public static final String  BASE_URL = "https://newsapi.org/v2/";
    public static final String APIKEY = "91549c93fcee4e4fb07dd79ce7c048a5";

    public static final String US = "us";
    public static final String Business = "business";
    public static final String Key = "Key";
    public static final String PublishedAt = "publishedAt";
    public static final String Date = "yyyy-MM-dd";
}
