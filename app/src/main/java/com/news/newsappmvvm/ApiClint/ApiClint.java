package com.news.newsappmvvm.ApiClint;

import com.news.newsappmvvm.Api.NewsAPI;
import com.news.newsappmvvm.Data.DataManager;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClint {

    private static Retrofit retrofit;

    public static  Retrofit getretrfit(){
        if(retrofit == null){
            retrofit  = new Retrofit.Builder()
                    .baseUrl(DataManager.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }


}
