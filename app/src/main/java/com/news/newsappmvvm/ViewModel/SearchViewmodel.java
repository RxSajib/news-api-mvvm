package com.news.newsappmvvm.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.news.newsappmvvm.Model.Response;
import com.news.newsappmvvm.Repository.NewsSearchRepository;

import org.jetbrains.annotations.NotNull;

public class SearchViewmodel extends AndroidViewModel {

    private NewsSearchRepository newsSearchRepository;

    public SearchViewmodel(@NonNull @NotNull Application application) {
        super(application);

        newsSearchRepository = new NewsSearchRepository(application);
    }

    public LiveData<Response> getsearchdata(String key, String date, String sortby, String apikey){
        return  newsSearchRepository.searchdata(key, date, sortby, apikey);
    }
}
