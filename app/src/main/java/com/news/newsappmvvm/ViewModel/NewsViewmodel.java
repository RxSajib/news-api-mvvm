package com.news.newsappmvvm.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.news.newsappmvvm.ApiClint.ApiClint;
import com.news.newsappmvvm.Model.Response;
import com.news.newsappmvvm.Repository.NewsRepository;

import org.jetbrains.annotations.NotNull;

public class NewsViewmodel extends AndroidViewModel {

    private NewsRepository newsRepository;

    public NewsViewmodel(@NonNull @NotNull Application application) {
        super(application);
        newsRepository = new NewsRepository(application);
    }

    public LiveData<Response> getnews(String Country, String Category, String APiKey){
        return newsRepository.getdata(Country, Category, APiKey);
    }


}
