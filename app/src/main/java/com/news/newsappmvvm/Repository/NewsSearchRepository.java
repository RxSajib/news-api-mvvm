package com.news.newsappmvvm.Repository;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.news.newsappmvvm.Api.NewsAPI;
import com.news.newsappmvvm.ApiClint.ApiClint;
import com.news.newsappmvvm.Model.Response;

import retrofit2.Call;
import retrofit2.Callback;

public class NewsSearchRepository {

    private Application application;
    private MutableLiveData<Response> data;
    private NewsAPI newsAPI;

    public NewsSearchRepository(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        newsAPI = ApiClint.getretrfit().create(NewsAPI.class);
    }

    public LiveData<Response> searchdata(String key, String date, String sortby, String apikey){
        newsAPI.searchnews(key, date, sortby, apikey).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@Nullable Call<Response> call,@Nullable retrofit2.Response<Response> response) {
                if(response.isSuccessful()){
                    data.setValue(response.body());
                }
                else {
                    Toast.makeText(application, "Response :"+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@Nullable Call<Response> call,@Nullable Throwable t) {
                Toast.makeText(application, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return data;
    }
}
