package com.news.newsappmvvm.Repository;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.news.newsappmvvm.Api.NewsAPI;
import com.news.newsappmvvm.ApiClint.ApiClint;
import com.news.newsappmvvm.Model.Response;

import retrofit2.Call;
import retrofit2.Callback;

public class NewsRepository {

    private Application application;
    private NewsAPI newsAPI;
    private MutableLiveData<Response> data;


    public NewsRepository(Application application){
        this.application = application;

        newsAPI = ApiClint.getretrfit().create(NewsAPI.class);
        data = new MutableLiveData<>();
    }

    public MutableLiveData<Response> getdata(String Country, String Category, String APiKey){
        newsAPI.gettop_news(Country, Category, APiKey)
                .enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(@Nullable Call<Response> call,@Nullable retrofit2.Response<Response> response) {
                        if(response.isSuccessful()){
                            data.setValue(response.body());

                        }
                    }

                    @Override
                    public void onFailure(@Nullable Call<Response> call, @Nullable Throwable t) {
                        data.setValue(null);
                    }
                });

        return data;
    }
}
