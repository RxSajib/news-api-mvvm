package com.news.newsappmvvm.Api;

import com.news.newsappmvvm.Model.Response;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsAPI {

    @GET("top-headlines")
    Call<Response> gettop_news(
            @Query("country") String country,
            @Query("category") String category,
            @Query("apiKey") String APiKey
    );


    @GET("everything")
    Call<Response> searchnews(
            @Query("q") String keyword,
            @Query("from") String date,
            @Query("sortBy") String sortby,
            @Query("apiKey") String apikey
    );

}
