package com.news.newsappmvvm.Page;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.news.newsappmvvm.Data.DataManager;
import com.news.newsappmvvm.Model.Articles;
import com.news.newsappmvvm.Model.Response;
import com.news.newsappmvvm.R;
import com.news.newsappmvvm.ViewModel.NewsViewmodel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.security.Key;
import java.util.ArrayList;
import java.util.List;

public class DetailsActivity extends AppCompatActivity {

    private int key;
    private List<Articles> articlesList  = new ArrayList<>();
    private LiveData<Response> newsViewmodel;
    private RelativeLayout backbutton;

    private ImageView newsimage;
    private TextView details, content, title, author;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        key = getIntent().getIntExtra(DataManager.Key, 0);

        Log.d("Key", String.valueOf(key));

        transprent_statusbar();
        init_view();
        getdata();
    }

    private void transprent_statusbar(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    private void init_view(){
        author = findViewById(R.id.Authortext);
        title = findViewById(R.id.TitleID);
        details = findViewById(R.id.DetailsID);
        content = findViewById(R.id.ContentID);
        newsimage = findViewById(R.id.ImageViewID);
        backbutton = findViewById(R.id.backButtonID);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getdata(){
        new ViewModelProvider(this).get(NewsViewmodel.class)
                .getnews(DataManager.US, DataManager.Business, DataManager.APIKEY)
                .observe(this, new Observer<Response>() {
                    @Override
                    public void onChanged(Response response) {

                        Picasso.get().load(response.getArticlesList().get(key).getUrlToImage()).into(newsimage, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError(Exception e) {

                            }
                        });

                        title.setText(response.getArticlesList().get(key).getTitle());
                        details.setText(response.getArticlesList().get(key).getDescription());
                        content.setText(response.getArticlesList().get(key).getContent());
                        author.setText(response.getArticlesList().get(key).getAuthor());
                    }
                });
    }
}