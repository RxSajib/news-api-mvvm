package com.news.newsappmvvm.Page;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.news.newsappmvvm.Adapter.MostPolularAdapter;
import com.news.newsappmvvm.Data.DataManager;
import com.news.newsappmvvm.Model.Articles;
import com.news.newsappmvvm.Model.Response;
import com.news.newsappmvvm.R;
import com.news.newsappmvvm.ViewModel.NewsViewmodel;

import java.util.ArrayList;
import java.util.List;

import carbon.view.View;

public class MainActivity extends AppCompatActivity {

    private NewsViewmodel newsViewmodel;
    private MostPolularAdapter mostPolularAdapter;
    private List<Articles> articlesList ;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private FloatingActionButton Searchbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        newsViewmodel = new ViewModelProvider(this).get(NewsViewmodel.class);
        init_view();

        getdata();
        click();
    }

    private void click(){
        Searchbutton.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                Intent intent = new Intent(getApplicationContext(), Searchpage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    private void init_view(){
        Searchbutton = findViewById(R.id.SearchButtoID);
        progressBar = findViewById(R.id.ProgressbarID);
        articlesList = new ArrayList<>();
        recyclerView = findViewById(R.id.MostPopularNewsRecylerviewID);
        mostPolularAdapter = new MostPolularAdapter();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(mostPolularAdapter);
    }

    private void getdata(){
        progressBar.setVisibility(View.VISIBLE);
        newsViewmodel.getnews(DataManager.US, DataManager.Business, DataManager.APIKEY)
                .observe(this, new Observer<Response>() {
                    @Override
                    public void onChanged(Response response) {
                        articlesList.addAll(response.getArticlesList());
                        mostPolularAdapter.setResponseList(articlesList);
                        mostPolularAdapter.notifyDataSetChanged();
                    //    Log.d("tag", response.getArticlesList().get(0).getAuthor());
                        progressBar.setVisibility(View.GONE);


                        mostPolularAdapter.setOnclickLisiner(new MostPolularAdapter.setOnclick() {
                            @Override
                            public void position(int position) {
                                Intent intent = new Intent(getApplicationContext(), DetailsActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra(DataManager.Key, position);
                                startActivity(intent);
                            }
                        });
                    }
                });
    }
}