package com.news.newsappmvvm.Page;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.util.Log;
import android.view.Window;

import com.news.newsappmvvm.Data.DataManager;
import com.news.newsappmvvm.Model.Response;
import com.news.newsappmvvm.R;
import com.news.newsappmvvm.Repository.NewsSearchRepository;
import com.news.newsappmvvm.ViewModel.SearchViewmodel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Searchpage extends AppCompatActivity {

    private SearchViewmodel searchViewmodel;
    private String CurrentDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.searchpage);

        getcurrentdate();
        searchViewmodel = new ViewModelProvider(this).get(SearchViewmodel.class);


        getdata();
    }

    private void getcurrentdate(){
        Calendar calendar_time = Calendar.getInstance(Locale.ENGLISH);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DataManager.Date);
        CurrentDate = simpleDateFormat.format(calendar_time.getTime());


        Log.d("Date", CurrentDate);
    }

    private void getdata(){
        searchViewmodel.getsearchdata("David Thomas", "2021-05-23", DataManager.PublishedAt, DataManager.APIKEY)
                .observe(this, new Observer<Response>() {
                    @Override
                    public void onChanged(Response response) {
                        Log.d("TAG", response.getArticlesList().get(1).getAuthor());
                    }
                });
    }
}