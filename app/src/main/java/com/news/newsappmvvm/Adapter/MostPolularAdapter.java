package com.news.newsappmvvm.Adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.news.newsappmvvm.Model.Articles;
import com.news.newsappmvvm.Model.Response;
import com.news.newsappmvvm.Page.DetailsActivity;
import com.news.newsappmvvm.R;
import com.news.newsappmvvm.ViewHolder.MostPopularViewHolder;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MostPolularAdapter extends RecyclerView.Adapter<MostPopularViewHolder> {

    private List<Articles> responseList;
    private setOnclick setOnclick;

    public void setResponseList(List<Articles> responseList) {
        this.responseList = responseList;
    }

    @NonNull
    @NotNull
    @Override
    public MostPopularViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_single_iteam, parent, false);
        return new MostPopularViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull MostPopularViewHolder holder, int position) {

        Picasso.get().load(responseList.get(position).getUrlToImage()).placeholder(R.drawable.newspaper).into(holder.newsimage, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {

            }
        });

        holder.detailsnews.setText(responseList.get(position).getDescription());
        holder.titlenews.setText(responseList.get(position).getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                Intent intent = new Intent(holder.itemView.getContext(), DetailsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                holder.itemView.getContext().startActivity(intent);
            }
        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOnclick.position(holder.getAdapterPosition());
            }
        });

    }

    @Override
    public int getItemCount() {
        if(responseList == null){
            return 0;
        }
        else {
          return  responseList.size();
        }
    }



    public interface setOnclick{
        void position(int position);
    }

    public void setOnclickLisiner(setOnclick setOnclick){
        this.setOnclick = setOnclick;
    }

}
