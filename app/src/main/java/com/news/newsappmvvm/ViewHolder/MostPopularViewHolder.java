package com.news.newsappmvvm.ViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.news.newsappmvvm.R;

import org.jetbrains.annotations.NotNull;

public class MostPopularViewHolder extends RecyclerView.ViewHolder {

    public RoundedImageView newsimage;
    public TextView detailsnews;
    public TextView titlenews;
    public TextView newstime;
    public Onclick Onclick;

    public MostPopularViewHolder(@NonNull @NotNull View itemView) {
        super(itemView);

        newsimage = itemView.findViewById(R.id.NewsImage);
        detailsnews = itemView.findViewById(R.id.DetailsTextID);
        titlenews = itemView.findViewById(R.id.TitleNewsID);
        newstime = itemView.findViewById(R.id.DateBox);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }


    public interface Onclick{
        void  Click(int position);
    }

    public void setOnclickLinier(Onclick Onclick){
        this.Onclick = Onclick;
    }
}
